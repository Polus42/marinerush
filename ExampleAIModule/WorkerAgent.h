#pragma once
#include "BaseAgent.h"
#include "GameManager.h"
class WorkerAgent : public BaseAgent
{
public:
	WorkerAgent(Unit &unit);
	~WorkerAgent();
	void mineGaz();
	void mineMinerals();
	bool build(UnitType building);
	void move();
	void attack();
	void runAway();
	virtual void computeActions();
};

