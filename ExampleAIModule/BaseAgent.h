#pragma once
#include <BWAPI.h>
#include <vector>

using namespace BWAPI;
class BaseAgent
{
protected:
	Unit unit;
public:
	BaseAgent(Unit &unit);
	~BaseAgent();
	virtual void computeActions();
};

