#include "ScoutAgent.h"


ScoutAgent::ScoutAgent(Unit &unit, bool mineralS) : BaseAgent(unit), mineralScount(mineralS)
{

}

void ScoutAgent::Init()
{
	if (mineralScount)
	{
		for each (Position var in GameManager::instance().mineralPositions)
		{
			//Broodwar << "positions: " << var << std::endl;
			mineralPositions.push_back(var);
			
		}
	}
}


ScoutAgent::~ScoutAgent()
{
}
void ScoutAgent::computeActions()
{
	//Broodwar << mineralPositions.size() << std::endl;
	Broodwar->drawBoxMap(Position(GameManager::instance().possibleScoutPositions.back()),
		Position(GameManager::instance().possibleScoutPositions.back() + Position(20,20)),
		Colors::Blue);
	if (scouting)
	{
		//Checking if it is moving
		if (!unit->isMoving())
		{
			if (!mineralScount)
			{
				unit->move(findClosest());
			}
			else
			{
				unit->move(FindClosestMineral());
			}
		}
		if (mineralScount)
		{
			if (unit->getPosition().getDistance(FindClosestMineral()) < 150)
			{
				mineralPositions.erase(std::remove(mineralPositions.begin(), mineralPositions.end(), FindClosestMineral()), mineralPositions.end());
				unit->move(FindClosestMineral());
			}
		}
		else
		{
			if (unit->getPosition().getDistance(findClosest()) < 150)
			{
				GameManager::instance().possibleScoutPositions.erase(std::remove(GameManager::instance().possibleScoutPositions.begin(), GameManager::instance().possibleScoutPositions.end(), findClosest()), GameManager::instance().possibleScoutPositions.end());
				unit->move(findClosest());
			}
		}
	}
	else
	{
		for (Task& t : GameManager::instance().tasks)
		{
			if (t.type == SCOUT)
			{
				GameManager::instance().consumeTask(t);
				scouting = true;
				break;
			}
		}
	}
}

Position ScoutAgent::findClosest()
{
	Position chosen = GameManager::instance().possibleScoutPositions[0];
	for (Position p : GameManager::instance().possibleScoutPositions)
	{
		if (p.getDistance(unit->getPosition()) < p.getDistance(chosen))
			chosen = p;
	}
	return chosen;
}


Position ScoutAgent::FindClosestMineral()
{
	Position pos = mineralPositions[0];
	for (Position p : mineralPositions)
	{
		if (p.getDistance(unit->getPosition()) < p.getDistance(pos))
		{
			pos = p;
		}
	}
	return pos;
}