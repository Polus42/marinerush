#include "GameManager.h"


GameManager::GameManager()
{

}
void GameManager::init()
{
	for (auto unit : Broodwar->self()->getUnits())
	{
		if (unit->getType() == UnitTypes::Terran_Command_Center)
		{
			commandCenter = unit;
		}
	}
	for (auto unit : Broodwar->getMinerals()){
		//Broodwar << "Pos:" << unit->getPosition() << std::endl;
		mineralPositions.push_back(unit->getPosition());
	}
	enemyBase = commandCenter->getPosition();
	numberOfWorker = 0;
	//tasks.push_back(Task(BUILD_BARRACK, 16));
	//tasks.push_back(Task(BUILD_BARRACK, 16));
	//tasks.push_back(Task(BUILD_BARRACK, 16));

	//tasks.push_back(Task(PRODUCE_WORKER, 20));
	//tasks.push_back(Task(PRODUCE_WORKER, 19));
	//tasks.push_back(Task(PRODUCE_WORKER, 18));
	//tasks.push_back(Task(PRODUCE_WORKER, 17));
	//tasks.push_back(Task(PRODUCE_WORKER, 16));
	//tasks.push_back(Task(PRODUCE_WORKER, 15));
	//tasks.push_back(Task(PRODUCE_WORKER, 14));
	//tasks.push_back(Task(PRODUCE_WORKER, 13));
	//tasks.push_back(Task(PRODUCE_WORKER, 12));
	//tasks.push_back(Task(PRODUCE_WORKER, 11));
	//numberOfWorker += 10;

	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));
	//tasks.push_back(Task(PRODUCE_MARINE, 15));

	auto getPos =
		[](const BWAPI::TilePosition tp, const BWAPI::UnitType ut)
	{
		return Position(Position(tp) + Position((ut.tileWidth() * BWAPI::TILEPOSITION_SCALE) / 2, (ut.tileHeight() * BWAPI::TILEPOSITION_SCALE) / 2));
	};
	for each (TilePosition var in Broodwar->getStartLocations())
	{
		possibleScoutPositions.push_back(getPos(var, BWAPI::UnitTypes::Special_Start_Location));
	}
	sort();
}


GameManager::~GameManager()
{
}
// Used to compare tasks
bool compareTask(const Task& t1,const Task& t2)
{
	return t1.priority > t2.priority;
}
Task& GameManager::getFirstTask()
{
	sort();
	return tasks.front();
}
void GameManager::consumeTask(Task& task)
{
	auto it = std::find(tasks.begin(), tasks.end(), task);
	if (it != tasks.end())
		tasks.erase(it);
}
void GameManager::sort()
{
	tasks.sort(compareTask);
}
int GameManager::availableMinerals()
{
	return (Broodwar->self()->minerals() - reservedMinerals);
}
void GameManager::update()
{
	if (!contains(BUILD_DEPOT) && plannedSupply - Broodwar->self()->supplyUsed() / 2 < 5 && Broodwar->self()->supplyUsed() / 2 >= 8)
	{
		plannedSupply += 8;
		tasks.push_back(Task(BUILD_DEPOT, 23));
		sort();
	}
	if (!contains(BUILD_DEPOT) && lastDepotFrame!=-1 && Broodwar->getFrameCount() - lastDepotFrame>300 && Broodwar->getUnitsOnTile(lastDepotPosition, Filter::GetType == UnitTypes::Terran_Supply_Depot).size() <= 0)
	{
		//Broodwar << "Failed creating supply" << std::endl;
		reservedMinerals -= UnitTypes::Terran_Supply_Depot.mineralPrice();
		tasks.push_back(Task(BUILD_DEPOT, 23));
		sort();
	}
	static bool scoutingDone = false;
	if (Broodwar->self()->supplyUsed() / 2 > 10 && !scoutingDone)
	{
		scoutingDone = true;
		tasks.push_back(Task(SCOUT, 16));
	}
	// Producing marines

	if (!contains(PRODUCE_WORKER) && availableMinerals() > 50 && numberOfWorker < 15)
	{
		tasks.push_back(Task(PRODUCE_WORKER, 22));
		numberOfWorker++;
	}
	if (!contains(PRODUCE_MARINE) && availableMinerals() > 50 && numberOfWorker >= 15)
	{
		tasks.push_back(Task(PRODUCE_MARINE, 20));
		sort();
	}
	if (!contains(BUILD_BARRACK) && availableMinerals() > 150)
	{
		tasks.push_back(Task(BUILD_BARRACK, 21));
		sort();
	}
}
int GameManager::remainingTasks()
{
	return tasks.size();
}
bool GameManager::contains(TaskTypes tt)
{
	for (Task t : tasks)
	{
		if (t.type == tt)
			return true;
	}
	return false;
}
