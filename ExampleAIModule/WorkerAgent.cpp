#include "WorkerAgent.h"

WorkerAgent::WorkerAgent(Unit &unit) : BaseAgent(unit)
{

}


WorkerAgent::~WorkerAgent()
{
}

void WorkerAgent::mineGaz()
{

}
void WorkerAgent::mineMinerals()
{
	auto getPos =
		[](const BWAPI::TilePosition tp, const BWAPI::UnitType ut)
	{
		return Position(Position(tp) + Position((ut.tileWidth() * BWAPI::TILEPOSITION_SCALE) / 2, (ut.tileHeight() * BWAPI::TILEPOSITION_SCALE) / 2));
	};
	// if our worker is idle
	if (unit->getTilePosition().getDistance(Broodwar->self()->getStartLocation())>50)
	{
		unit->move(getPos(Broodwar->self()->getStartLocation(), BWAPI::UnitTypes::Special_Start_Location));
	}
	else if (unit->isIdle())
	{
		// Order workers carrying a resource to return them to the center,
		// otherwise find a mineral patch to harvest.
		if (unit->isCarryingGas() || unit->isCarryingMinerals())
		{
			unit->returnCargo();
		}
		else if (!unit->getPowerUp())  // The worker cannot harvest anything if it
		{                             // is carrying a powerup such as a flag
			// Harvest from the nearest mineral patch or gas refinery
			if (!unit->gather(unit->getClosestUnit(Filter::IsMineralField || Filter::IsRefinery)))
			{
				// If the call fails, then print the last error message
				//Broodwar << Broodwar->getLastError() << std::endl;
			}

		} // closure: has no powerup
	} // closure: if idle
}
bool WorkerAgent::build(UnitType building)
{
	if (GameManager::instance().availableMinerals() > building.mineralPrice() && unit->canBuild(true) && unit->getTilePosition().getDistance(Broodwar->self()->getStartLocation())<300 && !unit->isMoving())
	{
		TilePosition targetBuildLocation = Broodwar->getBuildLocation(building, unit->getTilePosition());
		if (unit->build(building, targetBuildLocation))
		{
			//Broodwar << "Created " << building << std::endl;
			// Locking down those minerals
			GameManager::instance().reservedMinerals += building.mineralPrice();
			UnitType supplyProviderType = building;
			// Register an event that draws the target build location
			Broodwar->registerEvent([targetBuildLocation, supplyProviderType](Game*)
			{
				Broodwar->drawBoxMap(Position(targetBuildLocation),
					Position(targetBuildLocation + supplyProviderType.tileSize()),
					Colors::Blue);
			},
				nullptr,  // condition
				supplyProviderType.buildTime() + 100);
			if (building == UnitTypes::Terran_Supply_Depot)
			{
				GameManager::instance().lastDepotPosition = targetBuildLocation;
				GameManager::instance().lastDepotFrame = Broodwar->getFrameCount();
			}
			return true;
		}
		else
		{
			//Broodwar << "Failed building " << building << std::endl;
			//Broodwar << Broodwar->getLastError() << std::endl;
			return false;
		}
	}
	return false;
}
void WorkerAgent::computeActions()
{
	//checking what i do here
	bool didSomething = false;
	Task& task = GameManager::instance().getFirstTask();
		switch (task.type)
		{
		case BUILD_BARRACK:
			if (build(UnitTypes::Terran_Barracks))
			{
				GameManager::instance().consumeTask(task);
				didSomething = true;
			}
			break;
		case BUILD_DEPOT:
			if (build(UnitTypes::Terran_Supply_Depot))
			{
				GameManager::instance().consumeTask(task);
				didSomething = true;
			}
			break;
		case BUILD_REFINERY:
			break;
		default:
			if (!unit->isMoving())
			{
				mineMinerals();
				didSomething = true;
			}
			break;
		}
		if (!didSomething && !unit->isMoving())
			mineMinerals();
}