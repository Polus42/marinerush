#pragma once
#include "BaseAgent.h"
#include "GameManager.h"
class ScoutAgent :
	public BaseAgent
{
public:
	ScoutAgent(Unit &unit, bool mineralS = false);
	~ScoutAgent();
	virtual void computeActions();
	bool scouting = false;
	void Init();
private:
	Position findClosest();
	std::vector<Position> visitedLocation;
	bool mineralScount;
	std::vector<Position> mineralPositions;
	Position FindClosestMineral();

};

