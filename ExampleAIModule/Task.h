#pragma once

#include "BaseAgent.h"
enum TaskTypes
{
	BUILD_DEPOT,
	BUILD_BARRACK,
	BUILD_REFINERY,
	BUILD_ACADEMY,
	SCOUT,
	ATTACK_BASE,
	PRODUCE_WORKER,
	PRODUCE_MARINE
};
class Task
{
public:
	Task(TaskTypes type,int priority);
	~Task();
	int priority;
	std::vector<BaseAgent> affectedAgents;
	TaskTypes type;
	bool operator==(const Task& other) const;
};

