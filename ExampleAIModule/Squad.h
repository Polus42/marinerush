#pragma once
#include <BWAPI.h>
#include <vector>

using namespace BWAPI;

class Squad
{
public:
	BWAPI::Unitset squad;
	enum State{
		CLEAR, ENEMY_IN_RANGE
	} state;
	enum Order{
		ATTACK, MOVE, NONE
	} squad_order;

	BWAPI::Position target;

	BWAPI::Unitset killthem;
	
	bool isFull;
public:
	Squad();
	~Squad();

	void Process();
	bool isEnemyInRange();
	void Attack(Position pos);
	void Move(Position pos);
	void Add(Unit &u);
	void MicroKill(Unit &u);
	int Size() { return squad.size(); }
	bool Full();
};

