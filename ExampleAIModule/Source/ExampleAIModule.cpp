#include "ExampleAIModule.h"
#include <iostream>

using namespace BWAPI;
using namespace Filter;

void ExampleAIModule::onStart()
{
  // Hello World!
  Broodwar->sendText("Hello sir. I hope we have a good time, a lot of fun and a great game. You sound like a classy fella. <3");

  // Print the map name.
  // BWAPI returns std::string when retrieving a string, don't forget to add .c_str() when printing!
  Broodwar << "The map is " << Broodwar->mapName() << "!" << std::endl;

  // Enable the UserInput flag, which allows us to control the bot and type messages.
  Broodwar->enableFlag(Flag::UserInput);

  // Uncomment the following line and the bot will know about everything through the fog of war (cheat).
  //Broodwar->enableFlag(Flag::CompleteMapInformation);

  // Set the command optimization level so that common commands can be grouped
  // and reduce the bot's APM (Actions Per Minute).
  Broodwar->setCommandOptimizationLevel(2);

  // Check if this is a replay
  if ( Broodwar->isReplay() )
  {

    // Announce the players in the replay
    Broodwar << "The following players are in this replay:" << std::endl;
    
    // Iterate all the players in the game using a std:: iterator
    Playerset players = Broodwar->getPlayers();
    for(auto p : players)
    {
      // Only print the player if they are not an observer
      if ( !p->isObserver() )
        Broodwar << p->getName() << ", playing as " << p->getRace() << std::endl;
    }

  }
  else // if this is not a replay
  {
    // Retrieve you and your enemy's races. enemy() will just return the first enemy.
    // If you wish to deal with multiple enemies then you must use enemies().
    if ( Broodwar->enemy() ) // First make sure there is an enemy
      Broodwar << "The matchup is " << Broodwar->self()->getRace() << " vs " << Broodwar->enemy()->getRace() << std::endl;
  }
  for (auto &u : Broodwar->self()->getUnits())
  {
	  if (u->getType().isWorker())
	  {
		  agents.push_back(new WorkerAgent((Unit)u));
	  }
			if (u->getType().isBuilding())
			{
				agents.push_back(new BuildingAgent((Unit)u));
			}
  }
  GameManager::instance().init();
}

void ExampleAIModule::onEnd(bool isWinner)
{
  // Called when the game ends
  if ( isWinner )
  {
	  Broodwar->sendText("LOL U NOOB");
  }
}
void ExampleAIModule::onFrame()
{
	GameManager::instance().update();
  // Display the game frame rate as text in the upper left area of the screen
  Broodwar->drawTextScreen(200, 0,  "FPS: %d", Broodwar->getFPS() );
  Broodwar->drawTextScreen(200, 10, "Average FPS: %f", Broodwar->getAverageFPS() );
  Broodwar->drawTextScreen(200, 20, "APM: %d", Broodwar->getAPM());
  Broodwar->drawTextScreen(200, 30, "Pending Tasks: %d", GameManager::instance().remainingTasks());
  Broodwar->drawTextScreen(200, 40, "Doing : %d", GameManager::instance().getFirstTask().type);
  // Return if the game is a replay or is paused
  if ( Broodwar->isReplay() || Broodwar->isPaused() || !Broodwar->self() )
    return;
  // Prevent spamming by only running our onFrame once every number of latency frames.
  // Latency frames are the number of frames before commands are processed.
  if ( Broodwar->getFrameCount() % Broodwar->getLatencyFrames() != 0 )
    return;

  for (auto &squad : armies)
  {
	  Unitset attackers = GameManager::instance().commandCenter->getUnitsInRadius(300, Filter::IsEnemy && !Filter::IsWorker);
	if (attackers.size() > 0)
	{
		Broodwar->sendText(std::string("lul keK").c_str());
		for (Squad sq : armies)
		{
			sq.Attack(attackers.getPosition());
		}
	}
	  else if (squad.Size() >= 50 || squad.isFull)
	  {
		  bool found = false;
			  while (found == false && GameManager::instance().enemyBuildings.size() > 0){
				  Position target = GameManager::instance().enemyBuildings[0];
				  if (squad.squad.getPosition().getDistance(target) < 150 && squad.squad.getUnitsInRadius(150, Filter::IsBuilding && Filter::IsEnemy).size() <= 0){
					  GameManager::instance().enemyBuildings.erase(GameManager::instance().enemyBuildings.begin());
				  }
				  else{
					  squad.Attack(target);
					  found = true;
				  }
			  }
		  
	  }
		squad.Process();
  }
  for (int i = 0; i < agents.size();i++)
  {
	  agents[i]->computeActions();
  }


  /*
  // Iterate through all the units that we own
  for (auto &u : Broodwar->self()->getUnits())
  {
    // Ignore the unit if it no longer exists
    // Make sure to include this block when handling any Unit pointer!
    if ( !u->exists() )
      continue;

    // Ignore the unit if it has one of the following status ailments
    if ( u->isLockedDown() || u->isMaelstrommed() || u->isStasised() )
      continue;

    // Ignore the unit if it is in one of the following states
    if ( u->isLoaded() || !u->isPowered() || u->isStuck() )
      continue;

    // Ignore the unit if it is incomplete or busy constructing
    if ( !u->isCompleted() || u->isConstructing() )
      continue;


    // Finally make the unit do some stuff!


    // If the unit is a worker unit
    if ( u->getType().isWorker() )
    {
      // if our worker is idle
      if ( u->isIdle() )
      {
        // Order workers carrying a resource to return them to the center,
        // otherwise find a mineral patch to harvest.
        if ( u->isCarryingGas() || u->isCarryingMinerals() )
        {
          u->returnCargo();
        }
        else if ( !u->getPowerUp() )  // The worker cannot harvest anything if it
        {                             // is carrying a powerup such as a flag
          // Harvest from the nearest mineral patch or gas refinery
          if ( !u->gather( u->getClosestUnit( IsMineralField || IsRefinery )) )
          {
            // If the call fails, then print the last error message
            Broodwar << Broodwar->getLastError() << std::endl;
          }

        } // closure: has no powerup
      } // closure: if idle

    }
    else if ( u->getType().isResourceDepot() ) // A resource depot is a Command Center, Nexus, or Hatchery
    {

      // Order the depot to construct more workers! But only when it is idle.
      if ( u->isIdle() && !u->train(u->getType().getRace().getWorker()) )
      {
        // If that fails, draw the error at the location so that you can visibly see what went wrong!
        // However, drawing the error once will only appear for a single frame
        // so create an event that keeps it on the screen for some frames
        Position pos = u->getPosition();
        Error lastErr = Broodwar->getLastError();
        Broodwar->registerEvent([pos,lastErr](Game*){ Broodwar->drawTextMap(pos, "%c%s", Text::White, lastErr.c_str()); },   // action
                                nullptr,    // condition
                                Broodwar->getLatencyFrames());  // frames to run

        // Retrieve the supply provider type in the case that we have run out of supplies
        UnitType supplyProviderType = u->getType().getRace().getSupplyProvider();
        static int lastChecked = 0;

        // If we are supply blocked and haven't tried constructing more recently
        if (  lastErr == Errors::Insufficient_Supply &&
              lastChecked + 400 < Broodwar->getFrameCount() &&
              Broodwar->self()->incompleteUnitCount(supplyProviderType) == 0 )
        {
          lastChecked = Broodwar->getFrameCount();

          // Retrieve a unit that is capable of constructing the supply needed
          Unit supplyBuilder = u->getClosestUnit(  GetType == supplyProviderType.whatBuilds().first &&
                                                    (IsIdle || IsGatheringMinerals) &&
                                                    IsOwned);
          // If a unit was found
          if ( supplyBuilder )
          {
            if ( supplyProviderType.isBuilding() )
            {
              TilePosition targetBuildLocation = Broodwar->getBuildLocation(supplyProviderType, supplyBuilder->getTilePosition());
              if ( targetBuildLocation )
              {
                // Register an event that draws the target build location
                Broodwar->registerEvent([targetBuildLocation,supplyProviderType](Game*)
                                        {
                                          Broodwar->drawBoxMap( Position(targetBuildLocation),
                                                                Position(targetBuildLocation + supplyProviderType.tileSize()),
                                                                Colors::Blue);
                                        },
                                        nullptr,  // condition
                                        supplyProviderType.buildTime() + 100 );  // frames to run

                // Order the builder to construct the supply structure
                supplyBuilder->build( supplyProviderType, targetBuildLocation );
              }
            }
            else
            {
              // Train the supply provider (Overlord) if the provider is not a structure
              supplyBuilder->train( supplyProviderType );
            }
          } // closure: supplyBuilder is valid
        } // closure: insufficient supply
      } // closure: failed to train idle unit

    }

  } // closure: unit iterator
  */
}

void ExampleAIModule::onSendText(std::string text)
{

  // Send the text to the game if it is not being processed.
  Broodwar->sendText("%s", text.c_str());


  // Make sure to use %s and pass the text as a parameter,
  // otherwise you may run into problems when you use the %(percent) character!

}

void ExampleAIModule::onReceiveText(BWAPI::Player player, std::string text)
{
  // Parse the received text
  Broodwar->sendText(std::string("Fuk U GG EZ GAEM").c_str());

}

void ExampleAIModule::onPlayerLeft(BWAPI::Player player)
{
  // Interact verbally with the other players in the game by
  // announcing that the other player has left.
  Broodwar->sendText("Goodbye %s!", player->getName().c_str());
}

void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{

  // Check if the target is a valid position
  if ( target )
  {
    // if so, print the location of the nuclear strike target
    Broodwar << "Nuclear Launch Detected at " << target << std::endl;
  }
  else 
  {
    // Otherwise, ask other players where the nuke is!
    Broodwar->sendText("Where's the nuke?");
  }

  // You can also retrieve all the nuclear missile targets using Broodwar->getNukeDots()!
}

void ExampleAIModule::onUnitDiscover(BWAPI::Unit unit)
{
	//When discovering the enemeyyy
	if (unit->getPlayer()->isEnemy(Broodwar->self()))
	{
		if (unit->getType() == UnitTypes::Terran_Command_Center || unit->getType() == UnitTypes::Protoss_Nexus || unit->getType() == UnitTypes::Zerg_Hatchery)
		{
			Broodwar->sendText(std::string("ur build EZ so bad dude... U daed.").c_str());
			GameManager::instance().enemyBase = unit->getPosition();
		}
		if (unit->getType().isBuilding()){
			GameManager::instance().enemyBuildings.push_back(unit->getPosition());
		}
	}
}

void ExampleAIModule::onUnitEvade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitShow(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitHide(BWAPI::Unit unit)
{
}
bool hasScout = false;
void ExampleAIModule::onUnitCreate(BWAPI::Unit unit)
{
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s creates a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
  if (unit->getType().isWorker())
  {
	  if (!hasScout)
	  {
		  agents.push_back(new ScoutAgent((Unit)unit));
		  hasScout = true;
	  }
	  else
		  agents.push_back(new WorkerAgent((Unit)unit));
  }
  else if (unit->getType().isBuilding())
  {
	  agents.push_back(new BuildingAgent((Unit)unit));
  }
  else if (unit->getType().canAttack())
  {
	  // scouting marine
	  
	  GameManager::instance().scoutCounter++;
	  if (GameManager::instance().scoutCounter % 50 == 0)
	  {
		  //Broodwar << "Created agent :" << GameManager::instance().scoutCounter << std::endl;
		  ScoutAgent* scout = new ScoutAgent(unit, true);
		  scout->Init();
		  agents.push_back(scout);
		  GameManager::instance().tasks.push_back(Task(SCOUT,20));
		  return;
	  }


	  //agents.push_back(new WarAgent((Unit)unit));
	  bool added = false;
	  for (auto &squad : armies)
	  {
		  if (!squad.Full())
		  {
			  //Broodwar << unit->getType() << std::endl;
			  squad.Add(unit);
			  added = true;
			  break;
		  }
	  }
	  if (!added)
	  {
		  armies.push_back(Squad());
		  armies.back().Add(unit);
	  }
  }
  if (unit->getType().isBuilding())
  {
	  if (unit->getType() == UnitTypes::Terran_Barracks || unit->getType() == UnitTypes::Terran_Supply_Depot)
	  GameManager::instance().reservedMinerals -= unit->getType().mineralPrice();
  }
}

void ExampleAIModule::onUnitDestroy(BWAPI::Unit unit)
{
	if (unit->getType() == UnitTypes::Terran_SCV)
	{
		GameManager::instance().numberOfWorker--;
	}
	if (unit->getType() == UnitTypes::Terran_Supply_Depot)
	{
		GameManager::instance().plannedSupply -= 8;
	}
	if (unit->getPlayer()->isEnemy(Broodwar->self()))
	{
		Broodwar->sendText(std::string("wut u gon do witout " + unit->getType().getName()).c_str());
	}
}

void ExampleAIModule::onUnitMorph(BWAPI::Unit unit)
{
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s morphs a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
}

void ExampleAIModule::onUnitRenegade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onSaveGame(std::string gameName)
{
  Broodwar << "The game was saved to \"" << gameName << "\"" << std::endl;
}

void ExampleAIModule::onUnitComplete(BWAPI::Unit unit)
{
}
