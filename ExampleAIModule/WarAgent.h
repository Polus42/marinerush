
#include "BaseAgent.h"
class WarAgent :
	public BaseAgent
{
public:
	WarAgent(Unit &unit);
	~WarAgent();
	virtual void computeActions();
};
