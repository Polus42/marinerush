#pragma once
#include "BaseAgent.h"
//#include "GameManager.h"
class GameManager;

class BuildingAgent : public BaseAgent
{
public:
	BuildingAgent(Unit &uni);
	~BuildingAgent();
	virtual void computeActions();
private:
	bool produceUnit(UnitType type);
};

