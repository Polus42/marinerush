#include "Squad.h"

using namespace BWAPI;

Squad::Squad() : isFull(false)
{
}


Squad::~Squad()
{
}

void Squad::Process(){

	for (auto& u : killthem){
		if (!u->exists())
			killthem.erase(u);
	}
	for (auto& u : squad){
		if (!u->exists())
			squad.erase(u);
	}

	if (isEnemyInRange())
		state = ENEMY_IN_RANGE;
	else if (killthem.size() <=0)
		state = CLEAR;
	if (state == CLEAR)
	{
		if (squad_order == ATTACK && Broodwar->getFrameCount() % 30 == 0)
		{
			squad.attack(target);
		}
		/*switch (state){
		case CLEAR:
			if (squad_order == ATTACK)
			{
				squad.attack(target);
			}
			break;
		case ENEMY_IN_RANGE:
			for (auto u : squad){
				MicroKill(u);
			}
			break;
		}*/
	}
	else if (state == ENEMY_IN_RANGE)
	{
		for (auto u : squad){
			MicroKill(u);
		}
	}
}

bool Squad::isEnemyInRange(){
	for (auto &u : squad){
		Unitset enemies = u->getUnitsInRadius(u->getType().sightRange(), Filter::IsEnemy);
		if (enemies.size()>0){
			killthem = enemies;
			return true;
		}
	}
	return false;
}

void Squad::Attack(Position pos){
	target = pos;
	squad_order = ATTACK;
}

void Squad::Move(Position pos){
	target = pos;
	squad_order = MOVE;
}

void Squad::Add(Unit &u){
	squad.emplace(u);
	if (squad.size() == 50)
	{
		Broodwar->sendText(std::string("I'm coming for you mota fuka ! Cyka blyat").c_str());
		isFull = true;
	}
}

void Squad::MicroKill(Unit &u)
{
		Unit closeEnemy = u->getClosestUnit(Filter::IsEnemy);
		bool kite(true);
		double dist = (u->getDistance(closeEnemy));
		double speed = (u->getType().topSpeed());
		double range = u->getType().groundWeapon().maxRange();

		double timeToEnter = std::max(0.0, (dist - range) / speed);
		if (timeToEnter >= u->getGroundWeaponCooldown()
			|| closeEnemy->getType().isBuilding()
			|| closeEnemy->getType().groundWeapon().maxRange() >= range
			|| closeEnemy->getType().airWeapon().maxRange() >= range)
		{
			kite = false;
		}
		if (kite)
		{
			Position fleePosition((u->getPosition() - closeEnemy->getPosition()) + u->getPosition());
			if(fleePosition != u->getTargetPosition())
				u->move(fleePosition);
		}
		else
		{
			if (u->getLastCommand().getType() != UnitCommandTypes::Attack_Move || u->isIdle())
				u->attack(closeEnemy->getPosition());
		}





}

bool Squad::Full()
{
	return isFull;
}
