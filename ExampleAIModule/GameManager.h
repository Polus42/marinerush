#pragma once
#include "Task.h"
#include "Squad.h"

#include <vector>

extern std::vector<Task> tasks;
class GameManager
{
public:
	static GameManager& instance()
	{
		static GameManager *instance = new GameManager();
		return *instance;
	}
	~GameManager();
	void sort();
	std::list<Task> tasks;
	int reservedMinerals = 0;
	int plannedSupply =10;
	int availableMinerals();
	void update();
	int remainingTasks();
	std::vector<BWAPI::Position> possibleScoutPositions;
	void init();
	Position enemyBase = Position(-1,-1);
	std::vector<BWAPI::Position> enemyBuildings;
	int target = 0;
	Task& getFirstTask();
	void consumeTask(Task& task);
	bool contains(TaskTypes tt);
	int numberOfWorker;
	Unit commandCenter;
	TilePosition lastDepotPosition;
	int lastDepotFrame = -1;
	int scoutCounter = 0;
	std::vector<Position> mineralPositions;

private:
	GameManager();
};

