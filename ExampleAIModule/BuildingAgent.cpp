#include "BuildingAgent.h"
#include "Task.h"
#include "GameManager.h"

BuildingAgent::BuildingAgent(Unit &unit) : BaseAgent(unit)
{
}


BuildingAgent::~BuildingAgent()
{
}
bool BuildingAgent::produceUnit(UnitType type)
{
	if (GameManager::instance().availableMinerals() > type.mineralPrice() && !unit->isTraining() && unit->train(type))
	{
		return true;
	}
	else
	{
		return false;
	}
}
void BuildingAgent::computeActions()
{
	Task& task = GameManager::instance().getFirstTask();
		if (!unit->isTraining())
		switch (task.type)
		{
		case PRODUCE_WORKER:
			if (produceUnit(UnitTypes::Terran_SCV))
			{
				// task finished
				GameManager::instance().consumeTask(task);
				GameManager::instance().numberOfWorker++;
			}
			break;
		case PRODUCE_MARINE :
			if (produceUnit(UnitTypes::Terran_Marine))
			{
				GameManager::instance().consumeTask(task);
			}
			break;
		default:
			break;
		}
}